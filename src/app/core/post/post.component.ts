import { Component, OnInit } from '@angular/core';

import { HttpService } from '../../shared/http/http.service';
import { Post, User } from '../../shared/types';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts: Post[] = [];
  users: User[] = [];

  constructor(private httpService: HttpService) {}

  ngOnInit() {
    this.initData();
  }

  async initData() {
    try {
      await this.httpService.get('posts').subscribe(posts => {
        this.posts = posts.map(el => {
          return {
            body: el.body,
            id: el.id,
            title: el.title,
            userId: el.userId
          };
        });
      });
      await this.httpService.get('users').subscribe(users => {
        this.users = users.map(el => {
          return {
            id: el.id,
            name: el.name,
            username: el.username,
            email: el.email,
            address: {
              street: el.address.street,
              suite: el.address.suite,
              city: el.address.city,
              zipcode: el.address.zipcode,
              geo: {
                lat: el.address.geo.lat,
                lng: el.address.geo.lng
              }
            },
            phone: el.phone,
            website: el.website,
            company: {
              name: el.company.name,
              catchPhrase: el.company.catchPhrase,
              bs: el.company.bs
            }
          };
        });
      });
    } catch (error) {
      throw new Error(error);
    }
  }
}
