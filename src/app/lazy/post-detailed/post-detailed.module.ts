import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostDetailedComponent } from './post-detailed.component';
import { PostDetailedService } from './shared/post-detailed.service';
import { ButtonBackModule } from '../../shared/button-back/button-back.module';

const routerConfig: Routes = [
  {
    component: PostDetailedComponent,
    path: '',
    resolve: {
      post: PostDetailedService
    }
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routerConfig), ButtonBackModule],
  declarations: [PostDetailedComponent],
  providers: [PostDetailedService]
})
export class PostDetailedModule {}
