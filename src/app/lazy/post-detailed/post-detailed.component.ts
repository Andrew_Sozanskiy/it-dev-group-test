import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpService } from '../../shared/http/http.service';
import { Comment, Post, User } from '../../shared/types';

@Component({
  selector: 'app-post-detailed',
  templateUrl: './post-detailed.component.html',
  styleUrls: ['./post-detailed.component.css']
})
export class PostDetailedComponent implements OnInit {
  post: Post;
  comments: Comment[] = [];
  users: User[] = [];

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpService
  ) {}

  ngOnInit() {
    this.post = this.route.snapshot.data.post;
    this.initData();
  }

  private async initData() {
    try {
      await this.httpService
        .get(`posts/${this.post.id}/comments`)
        .subscribe(comments => {
          this.comments = comments.map(el => {
            return {
              postId: el.postId,
              name: el.name,
              body: el.body,
              email: el.email,
              id: el.id
            };
          });
        });
      await this.httpService.get('users').subscribe(users => {
        this.users = users.map(el => {
          return {
            id: el.id,
            name: el.name,
            username: el.username,
            email: el.email,
            address: {
              street: el.address.street,
              suite: el.address.suite,
              city: el.address.city,
              zipcode: el.address.zipcode,
              geo: {
                lat: el.address.geo.lat,
                lng: el.address.geo.lng
              }
            },
            phone: el.phone,
            website: el.website,
            company: {
              name: el.company.name,
              catchPhrase: el.company.catchPhrase,
              bs: el.company.bs
            }
          };
        });
      });
    } catch (error) {
      throw new Error(error);
    }
  }
}
