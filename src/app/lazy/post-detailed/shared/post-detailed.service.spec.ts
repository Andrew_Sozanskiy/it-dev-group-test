import { TestBed, inject } from '@angular/core/testing';

import { PostDetailedService } from './post-detailed.service';

describe('PostDetailedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostDetailedService]
    });
  });

  it('should be created', inject([PostDetailedService], (service: PostDetailedService) => {
    expect(service).toBeTruthy();
  }));
});
