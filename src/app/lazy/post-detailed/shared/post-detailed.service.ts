import 'rxjs/add/observable/of';

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { HttpService } from '../../../shared/http/http.service';
import { Post } from '../../../shared/types';

@Injectable()
export class PostDetailedService implements Resolve<Post> {
  constructor(private httpService: HttpService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Post> {
    return this.httpService.get(`posts/${route.params.id}`);
  }
}
