import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { HttpService } from '../../../shared/http/http.service';
import { Post } from '../../../shared/types';

@Injectable()
export class UserPostsService implements Resolve<Post[]> {
  constructor(private httpService: HttpService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Post[]> {
    const params = new HttpParams().set('userId', route.queryParams.userId);
    return this.httpService.get('posts', { params });
  }
}
