import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post } from '../../shared/types';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {
  posts: Post[] = [];

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.posts = this.route.snapshot.data.posts;
  }
}
