import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ButtonBackModule } from '../../shared/button-back/button-back.module';
import { UserPostsService } from './shared/user-posts.service';
import { UserPostsComponent } from './user-posts.component';

const routerConfig: Routes = [
  {
    component: UserPostsComponent,
    path: '',
    resolve: {
      posts: UserPostsService
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routerConfig),
    ButtonBackModule
  ],
  declarations: [UserPostsComponent],
  providers: [UserPostsService]
})
export class UserPostsModule {}
