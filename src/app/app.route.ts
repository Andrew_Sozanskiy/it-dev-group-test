import { Routes } from '@angular/router';

import { PostComponent } from './core/post/post.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: PostComponent
  },
  {
    path: 'post/:id',
    loadChildren: './lazy/post-detailed/post-detailed.module#PostDetailedModule'
  },
  {
    path: 'posts',
    loadChildren: './lazy/user-posts/user-posts.module#UserPostsModule'
  }
];
