export class Post {
  body: string = '';
  id: number = 0;
  title: string = '';
  userId: number = 0;
}

export class User {
  id: number = 0;
  name: string = '';
  username: string = '';
  email: string = '';
  address: Address;
  phone: string = '';
  website: string = '';
  company: Company;
}

export class Address {
  street: string = '';
  suite: string = '';
  city: string = '';
  zipcode: string = '';
  geo: Geo;
}

export class Geo {
  lat: string = '';
  lng: string = '';
}

export class Company {
  name: string = '';
  catchPhrase: string = '';
  bs: string = '';
}

export class Comment {
  postId: number = 0;
  name: string = '';
  body: string = '';
  email: string = '';
  id: number = 0;
}
