import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ButtonBackComponent } from './button-back.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ButtonBackComponent],
  exports: [ButtonBackComponent]
})
export class ButtonBackModule {}
