import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class HttpService {
  constructor(private http: HttpClient) {}

  get(url: string, params?): Observable<any> {
    const root = 'https://jsonplaceholder.typicode.com/';
    return this.http.get(root + url, params);
  }
}
